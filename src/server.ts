import Koa from "koa";
import jwt from "koa-jwt";
import bodyParser from "koa-bodyparser";
import helmet from "koa-helmet";
import cors from "@koa/cors";
import winston from "winston";
import { createConnection } from "typeorm";
import "reflect-metadata";

import { logger } from "./logger";
import { config } from "./config";
import { unprotectedRouter } from "./unprotectedRoutes";
import { protectedRouter } from "./protectedRoutes";
import { cron } from "./cron";

import dotenv from "dotenv";

dotenv.config();

// create connection with database
// ON BASIS OF ENVIRONMENT VARIABLES; https://github.com/typeorm/typeorm/blob/master/docs/using-ormconfig.md
// note that its not active database connection
// TypeORM creates you connection pool and uses connections from pool on your requests
createConnection().then(async () => {

    const app = new Koa();

    // Provides important security headers to make your app more secure
    app.use(helmet());

    // Enable cors with default options
    app.use(cors());

    // Logger middleware -> use winston as logger (logging.ts with config)
    app.use(logger(winston));

    // This is mostly to get failed stuff logged
    app.use(async (ctx, next) => {
        return next().catch((err) => {
            if (401 == err.status) {
                ctx.status = 401;
                ctx.body = "Authentication Error";
            } else {
                throw err;
            }
        });
    });

    // Enable bodyParser with default options
    app.use(bodyParser());

    // these routes are NOT protected by the JWT middleware, also include middleware to respond with "Method Not Allowed - 405".
    app.use(unprotectedRouter.routes()).use(unprotectedRouter.allowedMethods());

    // JWT middleware -> below this line routes are only reached if JWT token is valid, secret as env variable
    // do not protect swagger-json and swagger-html endpoints
    app.use(jwt({ secret: config.jwtSecret }).unless({ path: [/^\/swagger-/] }));

    // These routes are protected by the JWT middleware, also include middleware to respond with "Method Not Allowed - 405".
    app.use(protectedRouter.routes()).use(protectedRouter.allowedMethods());

    // Register cron job to do any action needed
    cron.start();

    app.listen(config.port);

    // Chicken-and-egg problem
    winston.createLogger({
        level: "info",
        transports: [ new winston.transports.Console({
            format: winston.format.combine(
                winston.format.colorize(),
                winston.format.simple(),
            ),
        })],
    }).info(`Server running on port ${config.port} `);

}).catch((error: string) => console.log("TypeORM connection error: ", error));