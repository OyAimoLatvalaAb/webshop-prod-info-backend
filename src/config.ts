import dotenv from "dotenv";

dotenv.config({ path: ".env" });

export interface Config {
    port: number;
    debugLogging: boolean;
    jwtSecret: string;
    cronJobExpression: string;
}

const isDevMode = process.env.NODE_ENV == "development";

const config: Config = {
    port: +(process.env.PORT || 3000),
    debugLogging: isDevMode,
    jwtSecret: process.env.JWT_SECRET || "your-secret-whatever",
    cronJobExpression: "0 * * * *"
};

export { config };