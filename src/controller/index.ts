export { default as GeneralController } from "./GeneralController";
export { default as ProductController } from "./ProductController";
export { default as AuthController } from "./AuthController";