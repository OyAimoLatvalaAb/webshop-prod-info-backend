import { BaseContext } from "koa";
import { getManager, Repository } from "typeorm";
import { validate, ValidationError } from "class-validator";
import { request, summary, path, body, responsesAll, tagsAll, responses } from "koa-swagger-decorator";
import { Product, productSchema } from "../entity/Product";


@responsesAll({
    200: { description: "success"},
    400: { description: "bad request"},
    404: { description: "not found"},
    401: { description: "unauthorized, missing/wrong jwt token"}
})
@tagsAll(["Product"])

export default class ProductController {
    @request("get", "/product-controller/products")
    @summary("Find all products")
    public static async getProducts(ctx: BaseContext): Promise<void> {

        // get a product repository to perform operations with product
        const productRepository: Repository<Product> = getManager().getRepository(Product);

        // load all products
        const products: Product[] = await productRepository.find();

        // return OK status code and loaded products array
        ctx.status = 200;
        ctx.body = products;
    }

    @request("get", "/product-controller/products/{EAN}")
    @summary("Find product by EAN")
    @path({
        EAN: { type: "string", required: true, description: "EAN of product" }
    })
    public static async getProduct(ctx: BaseContext): Promise<void> {

        // get a product repository to perform operations with product
        const productRepository: Repository<Product> = getManager().getRepository(Product);

        // load product by ean
        const product: Product | undefined = await productRepository.findOne(ctx.params.EAN);

        if (product) {
            // return OK status code and loaded product object
            ctx.status = 200;
            ctx.body = product;
        } else {
            // return a BAD REQUEST status code and error message
            ctx.status = 404;
            ctx.body = "The product you are trying to retrieve doesn't exist in the db";
        }
    }

    @request("post", "/product-controller/products")
    @summary("Create a product")
    @body(productSchema)
    public static async createProduct(ctx: BaseContext): Promise<void> {

        // get a product repository to perform operations with product
        const productRepository: Repository<Product> = getManager().getRepository(Product);

        // build up entity product to be saved
        const productToBeSaved: Product = new Product();

        productToBeSaved.EAN = ctx.request.body.EAN;
        productToBeSaved.salePrice = +ctx.request.body.salePrice || null;
        productToBeSaved.widthCm = +ctx.request.body.widthCm || null;
        productToBeSaved.heightCm = +ctx.request.body.heightCm || null;
        productToBeSaved.lengthCm = +ctx.request.body.lengthCm || null;
        productToBeSaved.manufacturerWebUrl = ctx.request.body.manufacturerWebUrl || "";
        productToBeSaved.unit = ctx.request.body.unit || null;
        productToBeSaved.massG = +ctx.request.body.massG || null;
        productToBeSaved.text = ctx.request.body.text || "";
        productToBeSaved.catalogVisibility = ctx.request.body.catalogVisibility || null;
        if(ctx.request.body.deferredDelivery !== null) {
            productToBeSaved.deferredDelivery = ctx.request.body.deferredDelivery;
        }
        productToBeSaved.isFragile = ctx.request.body.isFragile;
        productToBeSaved.isDangerous = ctx.request.body.isDangerous;
        productToBeSaved.isLarge = ctx.request.body.isLarge;
        productToBeSaved.vakAmount = ctx.request.body.vakAmount;

        // validate product entity
        const errors: ValidationError[] = await validate(productToBeSaved); // errors is an array of validation errors

        if (errors.length > 0) {
            // return BAD REQUEST status code and errors array
            ctx.status = 400;
            ctx.body = errors;
        } else if (await productRepository.findOne({ EAN: productToBeSaved.EAN })) {
            // return BAD REQUEST status code and email already exists error
            ctx.status = 400;
            ctx.body = "The specified ean address already exists";
        } else {
            // save the product contained in the POST body
            const product = await productRepository.save(productToBeSaved);
            // return CREATED status code and updated product
            ctx.status = 201;
            ctx.body = product;
        }
    }
    @responses({
        201: "product created",
        400: "bad request",
    })

    @request("put", "/product-controller/products/{EAN}")
    @summary("Update a product")
    @path({
        EAN: { type: "string", required: true, description: "EAN of product" }
    })
    @body(productSchema)
    public static async updateProduct(ctx: BaseContext): Promise<void> {

        // get a product repository to perform operations with product
        const productRepository: Repository<Product> = getManager().getRepository(Product);

        // update the product by specified id
        // build up entity product to be updated
        const productToBeUpdated: Product = new Product();

        productToBeUpdated.EAN = ctx.params.EAN;
        productToBeUpdated.salePrice = ctx.request.body.salePrice === "" ? null : ctx.request.body.salePrice;
        productToBeUpdated.widthCm = ctx.request.body.widthCm === "" ? null : ctx.request.body.widthCm;
        productToBeUpdated.heightCm = ctx.request.body.heightCm === "" ? null : ctx.request.body.heightCm;
        productToBeUpdated.lengthCm = ctx.request.body.lengthCm === "" ? null : ctx.request.body.lengthCm;
        productToBeUpdated.manufacturerWebUrl = ctx.request.body.manufacturerWebUrl;
        productToBeUpdated.unit = ctx.request.body.unit === "" ? null : ctx.request.body.unit;
        productToBeUpdated.massG = ctx.request.body.massG === "" ? null : ctx.request.body.massG;
        productToBeUpdated.text = ctx.request.body.text === "" ? null : ctx.request.body.text;
        productToBeUpdated.catalogVisibility = ctx.request.body.catalogVisibility;
        if(ctx.request.body.deferredDelivery !== null) {
            productToBeUpdated.deferredDelivery = ctx.request.body.deferredDelivery;
        }
        productToBeUpdated.isFragile = ctx.request.body.isFragile;
        productToBeUpdated.isDangerous = ctx.request.body.isDangerous;
        productToBeUpdated.isLarge = ctx.request.body.isLarge;
        productToBeUpdated.vakAmount = ctx.request.body.vakAmount;

        // validate product entity
        const errors: ValidationError[] = await validate(productToBeUpdated); // errors is an array of validation errors

        if (errors.length > 0) {
            // return BAD REQUEST status code and errors array
            ctx.status = 400;
            ctx.body = errors;
        } else if (!await productRepository.findOne(productToBeUpdated.EAN)) {
            // check if a product with the specified id exists
            // return a BAD REQUEST status code and error message
            ctx.status = 400;
            ctx.body = "The product you are trying to update doesn't exist in the db";
        } else {
            // save the product contained in the PUT body
            const product = await productRepository.save(productToBeUpdated);
            // return CREATED status code and updated product
            ctx.status = 201;
            ctx.body = product;
        }
    }

    @request("delete", "/product-controller/products/{EAN}")
    @summary("Delete product by EAN")
    @path({
        EAN: { type: "string", required: true, description: "EAN of product" }
    })
    public static async deleteProduct(ctx: BaseContext): Promise<void> {

        // get a product repository to perform operations with product
        const productRepository = getManager().getRepository(Product);

        // find the product by specified id
        const productToRemove: Product | undefined = await productRepository.findOne(ctx.params.EAN);
        if (!productToRemove) {
            // return a BAD REQUEST status code and error message
            ctx.status = 400;
            ctx.body = "The product you are trying to delete doesn't exist in the db";
        } else {
            // the product is there so can be removed
            await productRepository.remove(productToRemove);
            // return a NO CONTENT status code
            ctx.status = 204;
        }
    }
}
