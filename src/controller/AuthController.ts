import { BaseContext } from "koa";
import { request, summary, responsesAll, tagsAll } from "koa-swagger-decorator";

@responsesAll({
    200: { description: "success"},
    400: { description: "bad request"},
    404: { description: "not found"},
    401: { description: "unauthorized, missing/wrong jwt token"}
})
@tagsAll(["Meta"])
export default class AuthController {
    @request("get", "/meta/token-valid")
    @summary("Check JWT")
    public static async validateToken(ctx: BaseContext): Promise<void> {

        // return OK status code and loaded products array
        ctx.status = 200;
    }
}
