import { BaseContext } from "koa";
import { description, request, summary, tagsAll } from "koa-swagger-decorator";

@tagsAll(["General"])
export default class GeneralController {

    @request("get", "/")
    @summary("Application information page")
    @description("A simple page containing information about the application.")
    public static async appInformation(ctx: BaseContext): Promise<void> {
        ctx.body = {
            "name": "webshop-product-info",
            "version": process.env.npm_package_version
        };
    }

}