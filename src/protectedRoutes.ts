import { SwaggerRouter } from "koa-swagger-decorator";
import { ProductController, AuthController } from "./controller";
const protectedRouter = new SwaggerRouter();

// PRODUCT ROUTES
protectedRouter.get("/product-controller/products", ProductController.getProducts);
protectedRouter.get("/product-controller/products/:EAN", ProductController.getProduct);
protectedRouter.post("/product-controller/products", ProductController.createProduct);
protectedRouter.put("/product-controller/products/:EAN", ProductController.updateProduct);
protectedRouter.delete("/product-controller/products/:EAN", ProductController.deleteProduct);

protectedRouter.get("/auth/token-valid", AuthController.validateToken);

// Swagger endpoint
protectedRouter.swagger({
    title: "Webshop Product Info",
    description: "CRUD REST API for Webshop Product Info",
    version: `${process.env.version}`
});

// mapDir will scan the input dir, and automatically call router.map to all Router Class
protectedRouter.mapDir(__dirname);

export { protectedRouter };