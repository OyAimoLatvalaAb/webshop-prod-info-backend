import {MigrationInterface, QueryRunner} from "typeorm";

export class SaneDefaults1615831168867 implements MigrationInterface {
    name = "SaneDefaults1615831168867"

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE \"product\" ALTER COLUMN \"salePrice\" SET DEFAULT null");
        await queryRunner.query("ALTER TABLE \"product\" ALTER COLUMN \"widthCm\" SET DEFAULT null");
        await queryRunner.query("ALTER TABLE \"product\" ALTER COLUMN \"heightCm\" SET DEFAULT null");
        await queryRunner.query("ALTER TABLE \"product\" ALTER COLUMN \"lengthCm\" SET DEFAULT null");
        await queryRunner.query(`ALTER TABLE "product"
                                ALTER COLUMN "manufacturerWebUrl" TYPE TEXT,
                                ALTER COLUMN "manufacturerWebUrl" DROP NOT NULL,
                                ALTER COLUMN "manufacturerWebUrl" SET DEFAULT NULL`);
        await queryRunner.query("ALTER TABLE \"product\" ALTER COLUMN \"unit\" SET DEFAULT null");
        await queryRunner.query("ALTER TABLE \"product\" ALTER COLUMN \"massG\" SET DEFAULT null");
        await queryRunner.query(`ALTER TABLE "product"
                                ALTER COLUMN "text" TYPE TEXT,
                                ALTER COLUMN "text" DROP NOT NULL,
                                ALTER COLUMN "text" SET DEFAULT NULL`);
        // Can't rollback this
        await queryRunner.query("UPDATE \"product\" SET \"deferredDelivery\" = false WHERE \"deferredDelivery\" IS NULL");
        await queryRunner.query("ALTER TABLE \"product\" ALTER COLUMN \"deferredDelivery\" SET NOT NULL");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE \"product\" ALTER COLUMN \"deferredDelivery\" DROP NOT NULL");
        await queryRunner.query(`ALTER TABLE "product"
                                ALTER COLUMN "text" TYPE VARCHAR,
                                ALTER COLUMN "text" DROP NOT NULL,
                                ALTER COLUMN "text" DROP DEFAULT`);
        await queryRunner.query("ALTER TABLE \"product\" ALTER COLUMN \"massG\" DROP DEFAULT");
        await queryRunner.query("ALTER TABLE \"product\" ALTER COLUMN \"unit\" DROP DEFAULT");
        await queryRunner.query(`ALTER TABLE "product"
                                ALTER COLUMN "manufacturerWebUrl" TYPE VARCHAR,
                                ALTER COLUMN "manufacturerWebUrl" DROP NOT NULL,
                                ALTER COLUMN "manufacturerWebUrl" DROP DEFAULT`);
        await queryRunner.query("ALTER TABLE \"product\" ALTER COLUMN \"lengthCm\" DROP DEFAULT");
        await queryRunner.query("ALTER TABLE \"product\" ALTER COLUMN \"heightCm\" DROP DEFAULT");
        await queryRunner.query("ALTER TABLE \"product\" ALTER COLUMN \"widthCm\" DROP DEFAULT");
        await queryRunner.query("ALTER TABLE \"product\" ALTER COLUMN \"salePrice\" DROP DEFAULT");
    }

}
