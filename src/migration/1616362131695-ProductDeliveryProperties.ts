import {MigrationInterface, QueryRunner} from "typeorm";

export class ProductDeliveryProperties1616362131695 implements MigrationInterface {
    name = "ProductDeliveryProperties1616362131695"

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE \"product\" ADD \"isFragile\" boolean NOT NULL DEFAULT false");
        await queryRunner.query("ALTER TABLE \"product\" ADD \"isDangerous\" boolean NOT NULL DEFAULT false");
        await queryRunner.query("ALTER TABLE \"product\" ADD \"isLarge\" boolean NOT NULL DEFAULT false");
        await queryRunner.query("ALTER TABLE \"product\" ADD \"vakAmount\" numeric DEFAULT null");
        await queryRunner.query("ALTER TABLE \"product\" ALTER COLUMN \"salePrice\" SET DEFAULT null");
        await queryRunner.query("ALTER TABLE \"product\" ALTER COLUMN \"widthCm\" SET DEFAULT null");
        await queryRunner.query("ALTER TABLE \"product\" ALTER COLUMN \"heightCm\" SET DEFAULT null");
        await queryRunner.query("ALTER TABLE \"product\" ALTER COLUMN \"lengthCm\" SET DEFAULT null");
        await queryRunner.query("ALTER TABLE \"product\" ALTER COLUMN \"manufacturerWebUrl\" SET DEFAULT null");
        await queryRunner.query("ALTER TABLE \"product\" ALTER COLUMN \"unit\" SET DEFAULT null");
        await queryRunner.query("ALTER TABLE \"product\" ALTER COLUMN \"massG\" SET DEFAULT null");
        await queryRunner.query("ALTER TABLE \"product\" ALTER COLUMN \"text\" SET DEFAULT null");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE \"product\" ALTER COLUMN \"text\" DROP DEFAULT");
        await queryRunner.query("ALTER TABLE \"product\" ALTER COLUMN \"massG\" DROP DEFAULT");
        await queryRunner.query("ALTER TABLE \"product\" ALTER COLUMN \"unit\" DROP DEFAULT");
        await queryRunner.query("ALTER TABLE \"product\" ALTER COLUMN \"manufacturerWebUrl\" DROP DEFAULT");
        await queryRunner.query("ALTER TABLE \"product\" ALTER COLUMN \"lengthCm\" DROP DEFAULT");
        await queryRunner.query("ALTER TABLE \"product\" ALTER COLUMN \"heightCm\" DROP DEFAULT");
        await queryRunner.query("ALTER TABLE \"product\" ALTER COLUMN \"widthCm\" DROP DEFAULT");
        await queryRunner.query("ALTER TABLE \"product\" ALTER COLUMN \"salePrice\" DROP DEFAULT");
        await queryRunner.query("ALTER TABLE \"product\" DROP COLUMN \"vakAmount\"");
        await queryRunner.query("ALTER TABLE \"product\" DROP COLUMN \"isLarge\"");
        await queryRunner.query("ALTER TABLE \"product\" DROP COLUMN \"isDangerous\"");
        await queryRunner.query("ALTER TABLE \"product\" DROP COLUMN \"isFragile\"");
    }

}
