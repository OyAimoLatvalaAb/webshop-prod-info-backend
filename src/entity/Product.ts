import { Entity, Column, PrimaryColumn } from "typeorm";
import { IsOptional, MaxLength } from "class-validator";
import CatalogVisibility from "../types/CatalogVisibility";

@Entity()
export class Product {
    @PrimaryColumn({
        length: 32,
        unique: true,
    })
    EAN!: string;

    @Column({
        nullable: true,
        type: "decimal",
        default: null,
    })
    @IsOptional()
    salePrice: number | null = null;

    @Column({
        nullable: true,
        type: "decimal",
        default: null,
    })
    @IsOptional()
    widthCm: number | null = null;

    @Column({
        nullable: true,
        type: "decimal",
        default: null,
    })
    @IsOptional()
    heightCm: number | null = null;

    @Column({
        nullable: true,
        type: "decimal",
        default: null,
    })
    @IsOptional()
    lengthCm: number | null = null;

    @Column({
        nullable: true,
        default: null,
        type: "text",
    })
    @MaxLength(512)
    @IsOptional()
    manufacturerWebUrl: string | null = null;

    @Column({
        nullable: true,
        default: null,
    })
    unit!: string;

    @Column({
        nullable: true,
        type: "decimal",
        default: null,
    })
    @IsOptional()
    massG: number | null = null;

    @Column({
        nullable: true,
        default: null,
        type: "text",
    })
    @IsOptional()
    @MaxLength(4096)
    text: string | null = null;

    @Column({
        nullable: false,
        type: "enum",
        enum: CatalogVisibility,
        default: CatalogVisibility.visible,
    })
    catalogVisibility: CatalogVisibility = CatalogVisibility.visible;

    @Column({
        default: false,
        type: "boolean",
    })
    deferredDelivery = false;

    @Column({
        default: false,
        type: "boolean",
    })
    isFragile = false;

    @Column({
        default: false,
        type: "boolean",
    })
    isDangerous = false;

    @Column({
        default: false,
        type: "boolean",
    })
    isLarge = false;

    @Column({
        default: null,
        nullable: true,
        type: "decimal",
    })
    vakAmount: number | null = null;
}

export const productSchema = {
    EAN: { type: "string", required: true, example: "1337133713370" },
    salePrice: { type: "number", required: false, example: 32.20 },
    widthCm: { type: "number", required: false, example: 20 },
    heightCm: { type: "number", required: false, example: 20 },
    lengthCm: { type: "number", required: false, example: 20 },
    manufacturerWebUrl: { type: "string", required: false, example: "https://nedis.fi/tuote/something"},
    unit: { type: "string", required: true, example: "kpl" },
    massG: { type: "number", required: false, example: 200 },
    text: { type: "string", required: false, example: "Mahtava tuote yms yms" },
    catalogVisibility: { type: "enum", enum: CatalogVisibility, required: true, example: CatalogVisibility.visible },
    deferredDelivery: { type: "boolean", required: true, example: true },
};
