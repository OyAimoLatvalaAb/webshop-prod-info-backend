enum CatalogVisibility {
    visible = "visible",
    catalog = "catalog",
    search = "search",
    hidden = "hidden",
}

export default CatalogVisibility;
