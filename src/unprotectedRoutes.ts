import Router from "@koa/router";
import { GeneralController } from "./controller";

const unprotectedRouter = new Router();

// App Information route
unprotectedRouter.get("/", GeneralController.appInformation);

export { unprotectedRouter };