import { Context } from "koa";
import { config } from "./config";
import winston from "winston";

const logger = (winstonInstance: typeof winston)  => {
    winstonInstance.configure({
        level: config.debugLogging ? "debug" : "info",
        transports: [
            //
            // - Write all logs error (and below) to `error.log`.
            new winston.transports.File({ filename: "error.log", level: "error" }),
            //
            // - Write to all logs with specified level to console.
            new winston.transports.Console({
                format: winston.format.combine(
                    winston.format.colorize(),
                    winston.format.simple()
                )
            })
        ]
    });

    return async (ctx: Context, next: Function): Promise<void> => {

        const start = new Date().getTime();

        await next();

        const ms = new Date().getTime() - start;

        let logLevel: string;
        if (ctx.status >= 500) {
            logLevel = "error";
        } else if (ctx.status >= 400) {
            logLevel = "warn";
        } else {
            logLevel = "info";
        }

        let msg = `${ctx.method} ${ctx.originalUrl} ${ctx.status} ${ms}ms`;

        if(ctx.status >= 400) { // no good
            msg += ` ${ctx.message}: ${ctx.body}`;
        }

        winstonInstance.log(logLevel, msg);
    };
};

export { logger };