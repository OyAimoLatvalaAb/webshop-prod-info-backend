# Webshop Product Info Backend

[![Build Status](https://jenkins.alatvala.fi/buildStatus/icon?job=Oy+Aimo+Latvala+Ab%2Fwebshop-prod-info-backend%2Fmaster)](https://jenkins.alatvala.fi/job/Oy%20Aimo%20Latvala%20Ab/job/webshop-prod-info-backend/job/master/)
[![Quality Gate Status](https://sonar.alatvala.fi/api/project_badges/measure?project=webshop-prod-info-backend&metric=alert_status)](https://sonar.alatvala.fi/dashboard?id=webshop-prod-info-backend)
[![Maintainability Rating](https://sonar.alatvala.fi/api/project_badges/measure?project=webshop-prod-info-backend&metric=sqale_rating)](https://sonar.alatvala.fi/dashboard?id=webshop-prod-info-backend)
[![Reliability Rating](https://sonar.alatvala.fi/api/project_badges/measure?project=webshop-prod-info-backend&metric=reliability_rating)](https://sonar.alatvala.fi/dashboard?id=webshop-prod-info-backend)
[![Security Rating](https://sonar.alatvala.fi/api/project_badges/measure?project=webshop-prod-info-backend&metric=security_rating)](https://sonar.alatvala.fi/dashboard?id=webshop-prod-info-backend)
[![Bugs](https://sonar.alatvala.fi/api/project_badges/measure?project=webshop-prod-info-backend&metric=bugs)](https://sonar.alatvala.fi/dashboard?id=webshop-prod-info-backend)
[![Code Smells](https://sonar.alatvala.fi/api/project_badges/measure?project=webshop-prod-info-backend&metric=code_smells)](https://sonar.alatvala.fi/dashboard?id=webshop-prod-info-backend)
[![Coverage](https://sonar.alatvala.fi/api/project_badges/measure?project=webshop-prod-info-backend&metric=coverage)](https://sonar.alatvala.fi/dashboard?id=webshop-prod-info-backend)
[![Duplicated Lines (%)](https://sonar.alatvala.fi/api/project_badges/measure?project=webshop-prod-info-backend&metric=duplicated_lines_density)](https://sonar.alatvala.fi/dashboard?id=webshop-prod-info-backend)
[![Lines of Code](https://sonar.alatvala.fi/api/project_badges/measure?project=webshop-prod-info-backend&metric=ncloc)](https://sonar.alatvala.fi/dashboard?id=webshop-prod-info-backend)
[![Technical Debt](https://sonar.alatvala.fi/api/project_badges/measure?project=webshop-prod-info-backend&metric=sqale_index)](https://sonar.alatvala.fi/dashboard?id=webshop-prod-info-backend)
[![Vulnerabilities](https://sonar.alatvala.fi/api/project_badges/measure?project=webshop-prod-info-backend&metric=vulnerabilities)](https://sonar.alatvala.fi/dashboard?id=webshop-prod-info-backend)

## Background information

This project uses [javieraviles/node-typescript-koa-rest](https://github.com/javieraviles/node-typescript-koa-rest)
as a base. Please read its documentation for development. Additionally,
[rmariuzzo/markdown-swagger](https://github.com/rmariuzzo/markdown-swagger) has been added to generate
API endpoint documentation to markdown.

TODO: Actually implement the `markdown-swagger`. As `koa-swagger` seems to be a little particular in the sense
that it serves the docs with an API endpoint at `/swagger-html`, we don't have a `yaml` or `json` `swagger`
file to supply to `markdown-swagger`. Need to work around this by running `SwaggerRouter.dumpSwaggerJson`
somehow, don't have time for this at the moment :/.

## Usage

Please copy the `.example.env` file to `.env`, and modify the parameters to suite your environment.
This project uses `JWT` with `HS256` for authentication, and the `JWT` secret is placed into this `.env` file.
When accessing protected routes, a header in the form of `Bearer: JWT_CRED` must be set. You will have
to generate `JWT` creds using some tool, preferrably an offline one to not leak your secret to the internet. For
development purposes, you may use for instance [jwt.io](https://jwt.io) to generate your token. This is also
explained in the `node-typescript-koa-rest` documentation on github (above).

To build this project, run `yarn build`.

To run a development server, run `yarn watch-server`.

## Production deployment

Using prebuilt binaries, please install the runtime dependencies by issuing `yarn install --frozen-lockfile --prod`.
After this, please configure the runtime parameters by modifying `.example.env`, and renaming it to `.env`. Then,
make sure a database exists at the location you configured to `.env`, with a user which you also configured to `.env`.

TODO: check if user has to run `typeorm migrate` or something to initialize the database.

After this, the project may be ran with `PWD` pointing to the directory where `package.json` exists, by running `yarn start`.

TODO: Include package.json somehow sanely with prebuilt binaries. Or actually package this sanely with its deps.

## API endpoints

<!-- markdown-swagger -->

|method|endopint                          |description          |
|------|----------------------------------|---------------------|
|`GET`   |`/product-controller/products`      |Find all products    |
|`POST`  |`/product-controller/products`      |Create a product     |
|`GET`   |`/product-controller/products/{EAN}`|Find product by EAN  |
|`PUT`   |`/product-controller/products/{EAN}`|Update a product     |
|`DELETE`|`/product-controller/products/{EAN}`|Delete product by EAN|

<!-- /markdown-swagger -->
