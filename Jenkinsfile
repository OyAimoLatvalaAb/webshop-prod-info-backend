pipeline {
    agent {
        label 'nodejs'
    }
    stages {
        stage('Install Dependencies') {
            steps {
                sh 'yarn install --frozen-lockfile'
            }
        }
        stage('Linter') {
            steps {
                sh 'yarn lint'
            }
        }
        stage('Run tests') {
            steps {
                sh 'yarn test'
            }
        }
        stage('Static code analysis') {
            steps {
                withSonarQubeEnv('VonLatvala Sonar') {
                    script {
                        scannerHome = tool 'SonarScanner 4.0.0.1744'
                    }
                    sh "${scannerHome}/bin/sonar-scanner" +\
                        " -Dsonar.branch.name=\"${env.BRANCH_NAME}\"" +\
                        " -Dsonar.projectVersion=\$(jq -r < package.json .version)"
                }
            }
        }
        stage("Quality Gate") {
            steps {
                timeout(time: 10, unit: 'MINUTES') {
                    // Parameter indicates whether to set pipeline to UNSTABLE if Quality Gate fails
                    // true = set pipeline to UNSTABLE, false = don't
                    // Requires SonarQube Scanner for Jenkins 2.7+
                    waitForQualityGate abortPipeline: true
                }
            }
        }
        stage('Build Project') {
            steps {
                sh 'yarn build'
            }
        }
        stage('Package') {
            steps {
                sh 'tar cvzf "$(jq -r < package.json .name)-v$(jq -r < package.json .version).tar.gz" dist yarn.lock package.json'
            }
        }
        stage('Publish Artifacts') {
            when {
                expression { GIT_BRANCH == 'master' }
            }
            steps {
                script {
                    rtUpload (
                        serverId: 'VonLatvala Artifactory',
                        specPath: 'artifactFilespec.json',
                        failNoOp: true
                    )
                    rtPublishBuildInfo (
                        serverId: 'VonLatvala Artifactory',
                    )
                }
            }
        }
    }
}
